FROM ubuntu:18.04

WORKDIR /home/migracao_receitas
COPY . /home/migracao_receitas/

RUN chown -R 777 /etc/ /opt/ /root/ /home/ /mnt/
RUN chmod -R 777 /etc/ /opt/ /root/ /home/ /mnt/

RUN apt-get update
RUN apt-get -y install \
    sudo \
    curl \
    wget \
    unzip \
    git \
    vim \
    xterm \
    cron \
    python3.6 \
    python3.6-dev \
    python3-pip

RUN pip3 install -r /home/migracao_receitas/requirements.txt
RUN service cron start