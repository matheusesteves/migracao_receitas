import settings
from multicontent.publish import publish

def get_access_token():
    header = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    body = {
        'client_id': settings.CLIENT_ID,
        'client_secret': settings.CLIENT_SECRET,
        'grant_type': 'client_credentials'
    }

    token, status = publish(settings.URL_BASE_ACCOUNT_BACKSTAGE, body, header, None)
    return token['access_token']