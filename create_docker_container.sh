#!/bin/bash -xe

path=$(pwd)

user_name=`id -u -n`
user_id=`id -u`
user_group=100

docker rm -f migracao_receitas || true

docker run --privileged -it -d --name migracao_receitas \
    -v $path:/home/migracao_receitas \
    -e DUSERNAME=$user_name \
    -e DUSERID=$user_id \
    -e DUSERGROUP=$user_group \
    -p 80:80 \
    -p 8080:8080 \
    -p 9093:9093 \
    migracao_receitas bash

docker start migracao_receitas && docker exec -it migracao_receitas bash -c "./update_dependencies.sh"