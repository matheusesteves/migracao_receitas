#!/usr/bin/env python3

import requests
from multicontent.request_body import create_request_body
from multicontent.publish import publish
import sys
from jsonschema import validate
from security import security
import os
import settings
from settings import inject_env_variables
import json
from time import sleep
from logs.log import create_log

def find_multicontent(slug_core, migrated_recipes_file):
    for recipe_info in migrated_recipes_file:
        recipe_info_components = recipe_info.split(',')
        if recipe_info_components[0] == slug_core:
            return recipe_info_components[1]
    return None

def get_command_params():
    env_core = sys.argv[1]
    page = sys.argv[2]
    page_size = sys.argv[3]
    env_backstage = sys.argv[4]
    start_date = sys.argv[5]
    end_date = sys.argv[6]
    slug = None
    if len(sys.argv) == 8:
        slug = sys.argv[7]
    return env_core, page, page_size, env_backstage, start_date, end_date, slug

def open_log_files():
    logs_file = None
    migrated_recipes_file_write = None
    migrated_recipes_file_read = None
    if settings.ENV_BACKSTAGE == 'qa':
        logs_file = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/logs_qa.txt", "a+")
        migrated_recipes_file_write = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migrated_recipes_qa.txt", "a+")
        migrated_recipes_file_read = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migrated_recipes_qa.txt", "r")
    else:
        logs_file = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/logs_prod.txt", "a+")
        migrated_recipes_file_write = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migrated_recipes_prod.txt", "a+")
        migrated_recipes_file_read = open("/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migrated_recipes_prod.txt", "r")
    return logs_file, migrated_recipes_file_write, migrated_recipes_file_read

def create_header_authenticated():
    header = {
        'Authorization': 'Bearer {}'.format(security.get_access_token()),
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
    }
    return header

def create_migration_status(page, page_size, start_date, end_date, slug):
    migration_status = 'page: {}, pageSize: {}, startDate: {}, endDate: {}'.format(\
        page, page_size, start_date, end_date
    )
    if slug:
        migration_status = '{}, slug: {}'.format(migration_status, slug)
    return migration_status

def set_migration_status(status, env):
    migration_status_file = open('/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migration_status_{}.txt'.format(env), 'a+')
    migration_status_file.write('{}\n'.format(status))
    migration_status_file.close()

def find_migration_status(status, env):
    migration_status_file = None
    search_index = -1
    try:
        migration_status_file = open('/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migration_status_{}.txt'.format(env), 'r')
        if migration_status_file:
            index = 0
            for migration_status in migration_status_file:
                if migration_status.replace('\n', '') == status:
                    search_index = index
                    break
                index = index + 1
    except Exception as e:
        search_index = -1
    finally:
        if migration_status_file:
            migration_status_file.close()
    return search_index

def remove_migration_status(status, env):
    index_migration_status = find_migration_status(status, env)
    migration_status_file = None
    try:
        migration_status_file = open('/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migration_status_{}.txt'.format(env), 'r')
        if migration_status_file:
            status_list = migration_status_file.readlines()
            migration_status_file.close()
            migration_status_file = open('/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/logs/migration_status_{}.txt'.format(env), 'w')
            for migration_status in status_list:
                if status_list.index(migration_status) == index_migration_status:
                    migration_status_file.write('')
                else:
                    migration_status_file.write('{}\n'.format(migration_status))
    except Exception as e:
        pass
    finally:
        if migration_status_file:
            migration_status_file.close()

def main():
    env_core, page, page_size, env_backstage, start_date, end_date, slug = get_command_params()  
    migration_status = create_migration_status(page, page_size, start_date, end_date, slug)
    index_migration_status = find_migration_status(migration_status, env_backstage)

    if index_migration_status < 0:
        set_migration_status(migration_status, env_backstage)

        inject_env_variables(env_core, env_backstage)
        url_api_core = '{}servico/get_receitas_migracao?page={}&pageSize={}&startDate={}&endDate={}'.format(settings.URL_BASE_CORE, page, page_size, start_date, end_date)
        if slug:
            url_api_core = '{}&slug={}'.format(url_api_core, slug)
            
        url_api_backstage = settings.URL_BASE_API_BACKSTAGE + 'api/v2/multi-content/gshow'
        response_body = requests.get(url_api_core)
        response_json_list = response_body.json()
        SCHEMA = json.loads(open(os.path.abspath(os.path.join(
            os.path.dirname(__file__), "resources", "schema.json"))).read())
        logs_file, migrated_recipes_file_write, migrated_recipes_file_read = open_log_files()

        for recipe in response_json_list:
            error = ''
            multicontent_id = None
            try:
                multicontent_id = find_multicontent(recipe['slug'], migrated_recipes_file_read)
                multicontent = create_request_body(recipe, multicontent_id)
                multicontent_json = json.dumps(multicontent).encode('utf-8')
                response_json, response_status = publish(url_api_backstage, \
                    multicontent_json, create_header_authenticated(), multicontent_id)
            except Exception as exception:
                response_json = None
                response_status = None
                error = str(exception)
            finally:
                method = 'PUT' if multicontent_id else 'POST'
                create_log(recipe, response_json, response_status, logs_file, \
                    migrated_recipes_file_write, settings.ENV_CORE, settings.ENV_BACKSTAGE, \
                    error, method
                )

        remove_migration_status(migration_status, env_backstage)
        logs_file.close()
        migrated_recipes_file_write.close()
        migrated_recipes_file_read.close()
main()