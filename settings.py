CLIENT_ID = ''
CLIENT_SECRET = ''
DICT_TAGS = {}
URL_BASE_CORE = ''
URL_BASE_API_BACKSTAGE = ''
ENV_CORE = ''
ENV_BACKSTAGE = ''
URL_BASE_ACCOUNT_BACKSTAGE = ''
CONTENT_TYPE_ID = ''
CATEGORY_ID = ''

def inject_env_variables(env_core, env_backstage):
    global ENV_CORE
    global ENV_BACKSTAGE
    global URL_BASE_CORE
    global URL_BASE_API_BACKSTAGE
    global DICT_TAGS
    global CLIENT_ID
    global CLIENT_SECRET
    global URL_BASE_ACCOUNT_BACKSTAGE
    global CONTENT_TYPE_ID
    global CATEGORY_ID
    
    ENV_CORE = env_core
    ENV_BACKSTAGE = env_backstage
    RECIPE_MIGRATION_ENV_FLAG = 'CURRENTLY_RECIPE_BLOCK_MIGRATION_QA' if env_backstage == 'qa' else 'CURRENTLY_RECIPE_BLOCK_MIGRATION_PROD'
    CLIENT_ID = '2k92hlciO7+19yHeDcDURA==' if env_backstage == 'qa' else 'wQpGAlpaAB7SJvhC5jJ6Ww=='
    CLIENT_SECRET = 'M6TDgqjKO2W+3A4p70PJ2w==' if env_backstage == 'qa' else 'MC5Zn5S9K6Vpk8rYJ39EWQ=='
    CONTENT_TYPE_ID = 'bfea32f5-e5d4-4232-98a6-b14e3032e643' if env_backstage == 'qa' else 'bfea32f5-e5d4-4232-98a6-b14e3032e643'
    CATEGORY_ID = '4d30ff32-c23f-4cb9-9161-0f40b0d25fd3' if env_backstage == 'qa' else '4d30ff32-c23f-4cb9-9161-0f40b0d25fd3'
    URL_BASE_CORE = 'http://gnt.qa01.globoi.com/' if env_core == 'qa' else 'http://gnt.globo.com/'
    URL_BASE_ACCOUNT_BACKSTAGE = 'https://accounts.backstage.qa.globoi.com/token' if env_core == 'qa' else 'https://accounts.backstage.globoi.com/token'
    URL_BASE_API_BACKSTAGE = 'https://apis.backstage.qa.globoi.com/' if env_backstage == 'qa' else 'https://apis.backstage.globoi.com/'
    DICT_TAGS = {
        # Cozinha
        'Alemã':	'http://semantica.globo.com/gshow/Cozinha/21d0ccb3-313c-415b-a24a-3c7021c894a5',
        'Francesa':	'http://semantica.globo.com/gshow/Cozinha/8ece2f46-e002-4063-a953-f9fe10ebc47a',
        'Mexicana':	'http://semantica.globo.com/gshow/Cozinha/5ecdf073-4763-46ae-af3d-64618017e4fe',
        'Árabe':	'http://semantica.globo.com/gshow/Cozinha/b52800d2-78d4-49d0-921c-1afa212dad9a',
        'Chinesa':	'http://semantica.globo.com/gshow/Cozinha/96bfa310-7044-428d-bd18-ed51bc04cf11',
        'Portuguesa':	'http://semantica.globo.com/gshow/Cozinha/3d989e7a-233b-4094-9ba2-266ee32bd2a8',
        'Tailandesa':	'http://semantica.globo.com/gshow/Cozinha/3a72ff8b-a127-4157-b9e0-6a2ee07bfa0e',
        'Baiana':	'http://semantica.globo.com/gshow/Cozinha/b730d768-5f73-4f19-98f8-8d1c0cc6a2c6',
        'Gaúcha':	'http://semantica.globo.com/gshow/Cozinha/132d9d6b-bb90-42f5-a298-6ef9107d03ca',
        'Americana':	'http://semantica.globo.com/gshow/Cozinha/e26d7b62-6ce4-4f17-bfec-afc13dc09432',
        'Indiana':	'http://semantica.globo.com/gshow/Cozinha/6e02d7e0-845c-44ae-9711-b725099f2e6b',
        'Espanhola':	'http://semantica.globo.com/gshow/Cozinha/849ecc99-3cc5-48ab-9a7d-4d949e76a2b2',
        'Grega':	'http://semantica.globo.com/gshow/Cozinha/90632697-3cd0-45d9-9a61-f476bb6b6475',
        'Brasileira':	'http://semantica.globo.com/gshow/Cozinha/f89966f5-3884-43e9-b41c-2d300dbf51e2',
        'Nordestina':	'http://semantica.globo.com/gshow/Cozinha/89818f0b-a931-4593-b8d4-76698264d4b6',
        'Japonesa':	'http://semantica.globo.com/gshow/Cozinha/137c0e88-58c5-471a-bf6d-9eb0d6627869',
        'Mineira':	'http://semantica.globo.com/gshow/Cozinha/e8219c72-7baa-4c98-aeea-de62a0b4b50b',
        'Italiana':	'http://semantica.globo.com/gshow/Cozinha/c3129630-ef5a-4190-aab8-c674af99ec74',
        # Dietas
        'Vegetariana':	'http://semantica.globo.com/gshow/Dieta/117f93fd-4081-4a4f-b2e2-75296400fece',
        'Saudável':	'http://semantica.globo.com/gshow/Dieta/97d5398f-f1f7-433a-bf44-f18db3b9507d',
        'Sem lactose':	'http://semantica.globo.com/gshow/Dieta/109cc98a-25c9-462e-bab8-cafd7cd723f3',
        'Diet':	'http://semantica.globo.com/gshow/Dieta/cd043eb2-f7e7-484e-a3ba-44354fc94597',
        'Kosher':	'http://semantica.globo.com/gshow/Dieta/b4c693d0-42e7-456c-a282-0400f68c4dd2',
        'Macrobiótica':	'http://semantica.globo.com/gshow/Dieta/60ec4516-e93e-4455-91a6-9c28751849ad',
        'Baixo sódio':	'http://semantica.globo.com/gshow/Dieta/2bc38db7-7b3b-41ec-8b0a-dc4aade9d0c1',
        'Light':	'http://semantica.globo.com/gshow/Dieta/7595da42-3b67-485a-9380-5f11f53ec4a1',
        'Baixo colesterol':	'http://semantica.globo.com/gshow/Dieta/ec4b8b75-0588-45a4-8988-519f6c455d59',
        'Sem glúten':	'http://semantica.globo.com/gshow/Dieta/60b82b64-c6c5-4818-a87d-1a13252dc70b',
        'Desconta na comida - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/456ae611-2b8c-453d-bcd2-7a202d43cb22',
        'Boca nervosa - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/6eacb4b9-e30b-49d4-a174-52980d147825',
        'Não para nunca - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/a9da59e7-6f0f-461c-8cd7-b267e9b550da',
        'Fonte da Juventude':	'http://semantica.globo.com/gshow/Dieta/8e44e10e-9f33-4a15-bdc4-87bc1f3b8658',
        # ModoDePreparo
        'Ensopado':	'http://semantica.globo.com/gshow/ModoDePreparo/02d483b7-3e6c-49d3-85a3-8a6e008f83ce',
        'Frito':	'http://semantica.globo.com/gshow/ModoDePreparo/3ec7e5c9-b974-45e6-84d2-43d5c1ebe89e',
        'Vapor':	'http://semantica.globo.com/gshow/ModoDePreparo/3d1f0231-cd33-4ce3-81c5-659ebe1c7df0',
        'Assado':	'http://semantica.globo.com/gshow/ModoDePreparo/bc97020b-0c94-4fdd-9702-261b8d7402e2',
        'Cru':	'http://semantica.globo.com/gshow/ModoDePreparo/42b07472-a692-4029-81bd-95723ce32989',
        'Gelado e Congelado':	'http://semantica.globo.com/gshow/ModoDePreparo/d577d050-77cb-4be2-ac6a-65139a1eaa1a',
        'Grelhado':	'http://semantica.globo.com/gshow/ModoDePreparo/0905304c-45a0-472c-8f52-0b9743edb880',
        'Micro-ondas':	'http://semantica.globo.com/gshow/ModoDePreparo/a8bf645e-8927-4cab-8bc1-76c46e6de4de',
        'Refogado':	'http://semantica.globo.com/gshow/ModoDePreparo/d3312c33-69af-4c61-a38e-49d269383c70',
        'Cozido':	'http://semantica.globo.com/gshow/ModoDePreparo/511e4e26-d6b3-4a11-966d-4f040b87d7a2',
    } if env_backstage == 'qa' else {
        # Cozinha
        'Alemã':	'http://semantica.globo.com/gshow/Cozinha/21d0ccb3-313c-415b-a24a-3c7021c894a5',
        'Francesa':	'http://semantica.globo.com/gshow/Cozinha/8ece2f46-e002-4063-a953-f9fe10ebc47a',
        'Mexicana':	'http://semantica.globo.com/gshow/Cozinha/5ecdf073-4763-46ae-af3d-64618017e4fe',
        'Árabe':	'http://semantica.globo.com/gshow/Cozinha/b52800d2-78d4-49d0-921c-1afa212dad9a',
        'Chinesa':	'http://semantica.globo.com/gshow/Cozinha/96bfa310-7044-428d-bd18-ed51bc04cf11',
        'Portuguesa':	'http://semantica.globo.com/gshow/Cozinha/3d989e7a-233b-4094-9ba2-266ee32bd2a8',
        'Tailandesa':	'http://semantica.globo.com/gshow/Cozinha/3a72ff8b-a127-4157-b9e0-6a2ee07bfa0e',
        'Baiana':	'http://semantica.globo.com/gshow/Cozinha/b730d768-5f73-4f19-98f8-8d1c0cc6a2c6',
        'Gaúcha':	'http://semantica.globo.com/gshow/Cozinha/132d9d6b-bb90-42f5-a298-6ef9107d03ca',
        'Americana':	'http://semantica.globo.com/gshow/Cozinha/e26d7b62-6ce4-4f17-bfec-afc13dc09432',
        'Indiana':	'http://semantica.globo.com/gshow/Cozinha/6e02d7e0-845c-44ae-9711-b725099f2e6b',
        'Espanhola':	'http://semantica.globo.com/gshow/Cozinha/849ecc99-3cc5-48ab-9a7d-4d949e76a2b2',
        'Grega':	'http://semantica.globo.com/gshow/Cozinha/90632697-3cd0-45d9-9a61-f476bb6b6475',
        'Brasileira':	'http://semantica.globo.com/gshow/Cozinha/f89966f5-3884-43e9-b41c-2d300dbf51e2',
        'Nordestina':	'http://semantica.globo.com/gshow/Cozinha/89818f0b-a931-4593-b8d4-76698264d4b6',
        'Japonesa':	'http://semantica.globo.com/gshow/Cozinha/137c0e88-58c5-471a-bf6d-9eb0d6627869',
        'Mineira':	'http://semantica.globo.com/gshow/Cozinha/e8219c72-7baa-4c98-aeea-de62a0b4b50b',
        'Italiana':	'http://semantica.globo.com/gshow/Cozinha/c3129630-ef5a-4190-aab8-c674af99ec74',
        # Dietas
        'Vegetariana':	'http://semantica.globo.com/gshow/Dieta/117f93fd-4081-4a4f-b2e2-75296400fece',
        'Saudável':	'http://semantica.globo.com/gshow/Dieta/97d5398f-f1f7-433a-bf44-f18db3b9507d',
        'Sem lactose':	'http://semantica.globo.com/gshow/Dieta/109cc98a-25c9-462e-bab8-cafd7cd723f3',
        'Diet':	'http://semantica.globo.com/gshow/Dieta/cd043eb2-f7e7-484e-a3ba-44354fc94597',
        'Kosher':	'http://semantica.globo.com/gshow/Dieta/b4c693d0-42e7-456c-a282-0400f68c4dd2',
        'Macrobiótica':	'http://semantica.globo.com/gshow/Dieta/60ec4516-e93e-4455-91a6-9c28751849ad',
        'Baixo sódio':	'http://semantica.globo.com/gshow/Dieta/2bc38db7-7b3b-41ec-8b0a-dc4aade9d0c1',
        'Light':	'http://semantica.globo.com/gshow/Dieta/7595da42-3b67-485a-9380-5f11f53ec4a1',
        'Baixo colesterol':	'http://semantica.globo.com/gshow/Dieta/ec4b8b75-0588-45a4-8988-519f6c455d59',
        'Sem glúten':	'http://semantica.globo.com/gshow/Dieta/60b82b64-c6c5-4818-a87d-1a13252dc70b',
        'Desconta na comida - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/456ae611-2b8c-453d-bcd2-7a202d43cb22',
        'Boca nervosa - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/6eacb4b9-e30b-49d4-a174-52980d147825',
        'Não para nunca - série Ideal pra Você (Fantástico)':	'http://semantica.globo.com/gshow/Dieta/a9da59e7-6f0f-461c-8cd7-b267e9b550da',
        'Fonte da Juventude':	'http://semantica.globo.com/gshow/Dieta/8e44e10e-9f33-4a15-bdc4-87bc1f3b8658',
        # ModoDePreparo
        'Ensopado':	'http://semantica.globo.com/gshow/ModoDePreparo/02d483b7-3e6c-49d3-85a3-8a6e008f83ce',
        'Frito':	'http://semantica.globo.com/gshow/ModoDePreparo/3ec7e5c9-b974-45e6-84d2-43d5c1ebe89e',
        'Vapor':	'http://semantica.globo.com/gshow/ModoDePreparo/3d1f0231-cd33-4ce3-81c5-659ebe1c7df0',
        'Assado':	'http://semantica.globo.com/gshow/ModoDePreparo/bc97020b-0c94-4fdd-9702-261b8d7402e2',
        'Cru':	'http://semantica.globo.com/gshow/ModoDePreparo/42b07472-a692-4029-81bd-95723ce32989',
        'Gelado e Congelado':	'http://semantica.globo.com/gshow/ModoDePreparo/d577d050-77cb-4be2-ac6a-65139a1eaa1a',
        'Grelhado':	'http://semantica.globo.com/gshow/ModoDePreparo/0905304c-45a0-472c-8f52-0b9743edb880',
        'Micro-ondas':	'http://semantica.globo.com/gshow/ModoDePreparo/a8bf645e-8927-4cab-8bc1-76c46e6de4de',
        'Refogado':	'http://semantica.globo.com/gshow/ModoDePreparo/d3312c33-69af-4c61-a38e-49d269383c70',
        'Cozido':	'http://semantica.globo.com/gshow/ModoDePreparo/511e4e26-d6b3-4a11-966d-4f040b87d7a2',
    }
