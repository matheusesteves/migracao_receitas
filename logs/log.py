from datetime import datetime
import json

def create_log(recipe, response_json, status, logs_file, \
    migrated_recipes_file, env_core, env_backstage, error, method):
    try:
        actual_date_and_time = datetime.now()
        actual_date_and_time = actual_date_and_time.strftime('%d/%m/%Y %H:%M')
        log = 'Método: {}'.format(method)
        log = '{}, Id: {}, Status do processo: {}'.format(log, response_json['id'], status) if status == 201 else \
        '{}, Status do processo: {}'.format(log, status)
        log = '{}, Nome: {}, Slug: {}, Data de criacao: {}, Data de migração: {}, Ambiente GNT: {}, Ambiente GSHOW: {} \n'.format(log, recipe['title'], \
        recipe['slug'], recipe['created'], actual_date_and_time, env_core, env_backstage)
        logs_file.write(log)
        migrated_recipes_file.write('{},{}\n'.format(recipe['slug'], response_json['id']))
        return log
    except Exception as exception:
        pass