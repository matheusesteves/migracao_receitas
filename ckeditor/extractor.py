import re
import html
from megadraft.unstyled import create_block_unstyled, create_entity_map_link_item, \
    create_entity_range, create_inline_style_range
from megadraft.photo import create_block_photo
from megadraft.video import create_block_video

def extract_block_link_megadraft(body_core_link_component, key):
    link_url_search = re.search('<a.*?href="(.*?)".*?>', body_core_link_component)
    link_text_search = re.search('<a.*?>(.*?)</a>', body_core_link_component)
    link_url = link_url_search.group(1).strip() if link_url_search else ''
    link_text = link_text_search.group(1).strip() if link_text_search else ''
    
    entity_ranges = [create_entity_range(0, len(link_text), key)]
    entity_map_item = create_entity_map_link_item(link_url)

    return create_block_unstyled(link_text, [], entity_ranges), entity_map_item

def extract_block_photo_megadraft(body_core_image_component, caption):
    image_url_search = re.search('<img.*?src="(.*?)".*?>', body_core_image_component)
    image_width_search = re.search('<img.*?width="(.*?)".*?>', body_core_image_component)
    image_height_search = re.search('<img.*?height="(.*?)".*?>', body_core_image_component)
    image_url = image_url_search.group(1).strip() if image_url_search else ''
    image_width = int(image_width_search.group(1).strip()) if image_width_search else 0
    image_height = int(image_height_search.group(1).strip()) if image_height_search else 0
    
    return create_block_photo(image_url, image_width, image_height, caption)

def extract_block_video_megadraft(body_core_video_component):
    globoid_video_search = re.search('<div.*?videosids="(.*?)".*?', body_core_video_component)
    globoid_video = int(globoid_video_search.group(1).strip()) if globoid_video_search else None
    return create_block_video(globoid_video)

def extract_tag_styled_text(body_core_styled_text_component, style_tag_name):
    styled_text_search = re.search('<' + style_tag_name + '.*?>(.*?)</.*?>', body_core_styled_text_component)
    styled_text = styled_text_search.group(1).strip() if styled_text_search else ''
    return html.unescape(styled_text)

def extract_block_styled_text_megadraft(body_core_styled_text_component, style_name, style_tag_name):
    styled_text = extract_tag_styled_text(body_core_styled_text_component, style_tag_name)
    inline_style_range = create_inline_style_range(0, len(styled_text), style_name)
    return create_block_unstyled(styled_text, [inline_style_range], [])