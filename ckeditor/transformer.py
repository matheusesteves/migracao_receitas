import re

def transform_body_tags(recipe_body):
    return re.sub(re.compile('<br.*?/>'), '\n', recipe_body)

def remove_body_tags(body, replace_tags):
    formatted_body = body
    for tag in replace_tags:
        formatted_body = re.sub(re.compile(tag), '', formatted_body)
    return formatted_body

def format_body_tags(body, tags, mark_character, opening_tag):
    formatted_body = body
    for tag in tags:
        formatted_body = formatted_body.replace(tag, 
            mark_character + tag if opening_tag else tag + mark_character
        )
    return formatted_body

def mark_body_components(body, mark_character):
    clean_body = remove_body_tags(transform_body_tags(body), [
        '<div.*?class=".*?foto.*?>','</*.?div>', '\n', '\r', '\t'
    ])

    reference_opening_tags = ['<a', '<img', '<strong', '<iframe']
    reference_closing_tags = ['</a>', '/>', '</strong>', '</iframe>','</ a>', '</ strong>', '</ iframe>']

    marked_body = format_body_tags(clean_body, reference_opening_tags, mark_character, True)
    marked_body = format_body_tags(marked_body, reference_closing_tags, mark_character, False)
    marked_body = marked_body.strip().strip(mark_character).strip()

    mark_character_sub_format = "['" + mark_character +"']"
    marked_body_sub_format = "\s+" + mark_character_sub_format + "\s+" + mark_character_sub_format + "\s+"
    marked_body = re.sub(marked_body_sub_format, mark_character, marked_body)
    return marked_body.replace(mark_character+mark_character, mark_character)