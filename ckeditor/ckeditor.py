import re
import html
from ckeditor.transformer import mark_body_components
from ckeditor.extractor import extract_block_link_megadraft, extract_tag_styled_text, \
    extract_block_photo_megadraft, extract_block_styled_text_megadraft, \
    extract_block_video_megadraft
from megadraft.unstyled import create_block_unstyled

def is_component_link(body_core_component):
    return body_core_component[:2] == '<a'

def is_component_image(body_core_component):
    return body_core_component[:4] == '<img'

def is_component_bold_text(body_core_component):
    return body_core_component[:7] == '<strong'

def is_component_video(body_core_component):
    return 'videosids=' in body_core_component

def extract_blocks_megadraft(body_core_components):
    blocks_megadraft = []
    entity_map = {}
    currently_link_key = 0
    component_position = 0
    while component_position < len(body_core_components):
        body_core_component = body_core_components[component_position]
        block_megadraft = None
        if is_component_link(body_core_component):
            block_megadraft, entity_map_item = extract_block_link_megadraft(\
                body_core_component, currently_link_key)
            entity_map[str(currently_link_key)] = entity_map_item
            currently_link_key = currently_link_key + 1
        else:
            if is_component_image(body_core_component):
                next_body_core_component = body_core_components[component_position+1] \
                    if component_position+1 < len(body_core_components) else None
                caption = ''
                if next_body_core_component and is_component_bold_text(next_body_core_component):
                    caption = extract_tag_styled_text(next_body_core_component, 'strong')
                    component_position = component_position + 1
                block_megadraft = extract_block_photo_megadraft(body_core_component, caption)
            else:          
                if is_component_bold_text(body_core_component):
                    block_megadraft = extract_block_styled_text_megadraft(\
                        body_core_component, 'BOLD', 'strong')
                else:
                    if is_component_video(body_core_component):
                       block_megadraft = extract_block_video_megadraft(body_core_component)
                    else:
                        block_megadraft = create_block_unstyled(body_core_component, [], [])
        component_position = component_position + 1
        blocks_megadraft.append(block_megadraft)
    return blocks_megadraft, entity_map

def transform_ckeditor_core(recipe_body):
    mark_character = '^'
    marked_body = mark_body_components(recipe_body, mark_character)
    body_components = marked_body.split(mark_character)
    formatted_body_components = []
    for body_component in body_components:
        formatted_body_components.append(body_component.strip())

    return extract_blocks_megadraft(formatted_body_components)