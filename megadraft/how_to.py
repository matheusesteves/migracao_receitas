from megadraft.key_generator import generate_random_key

def create_items_block_steps(items_step):
    items_block_steps = []
    for item_step in items_step:
        content = item_step['content']
        items_block_steps.append({
            'key': generate_random_key(),
            'content': content
        })
    return items_block_steps

def create_block_how_to(block_steps):
    block_title = block_steps['titleHowTo']
    items_steps = block_steps['steps']
    block = {
        'key': generate_random_key(),
        'text':'',
        'type':'atomic',
        'depth':0,
        'inlineStyleRanges':[],
        'entityRanges':[],
        'data':{
            'type':'recipe-how-to',
            'titleHowto': block_title,
            'steps': create_items_block_steps(items_steps)
        }
    }
    return block