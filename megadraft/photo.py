from megadraft.key_generator import generate_random_key

def create_block_photo(url_photo, width, height, caption):
    return {
        'key': generate_random_key(),
        'text': '',
        'type': 'atomic',
        'depth': 0,
        'inlineStyleRanges': [],
        'entityRanges': [],
        'data':{
            'caption': caption,
            'type': 'backstage-photo',
            'rightsHolder': 'gcom',
            'display': 'normal',
            'file': {
                'url': url_photo,
                'width': width,
                'height': height,
                'crop': {
                    'top': 0,
                    'left': 0,
                    'bottom': height,
                    'right': width,
                    'width': width,
                    'height': height
                }
            }
        }
    }