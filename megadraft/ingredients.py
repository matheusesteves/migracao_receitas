from megadraft.key_generator import generate_random_key

def create_items_block_ingredients(items_ingredients):
    items_block_ingredients = []
    for item_ingredient in items_ingredients:
        material = item_ingredient['material']
        items_block_ingredients.append({
            'key': generate_random_key(),
            'material': material,
            'unit': '',
            'quantity': ''
        })
    return items_block_ingredients 

def create_block_ingredients(block_ingredients):
    block_title = block_ingredients['titleIngredients']
    items_ingredients = block_ingredients['itemsIngredients']
    block = {
        'key': generate_random_key(),
        'text':'',
        'type':'atomic',
        'depth':0,
        'inlineStyleRanges':[],
        'entityRanges':[],
        'data':{
            'type':'recipe-ingredients',
            'titleIngredients': block_title,
            'itemsIngredients': create_items_block_ingredients(items_ingredients)
        }
    }
    return block