from megadraft.key_generator import generate_random_key
import html

def create_block_unstyled(text, inline_styled_ranges, entity_ranges):
    return {
        'key': generate_random_key(),
        'text': html.unescape(text),
        'type': 'unstyled',
        'depth': 0,
        'inlineStyleRanges': inline_styled_ranges,
        'entityRanges': entity_ranges,
        'data': {}
    }

def create_entity_map_link_item(link_url):
    return {
        'type': 'LINK',
        'mutability': 'MUTABLE',
        'data': {
            'url': link_url
        }
    }

def create_entity_range(offset, length, key):
    return {
        'offset': offset,
        'length': length,
        'key': key
    }

def create_inline_style_range(offset, length, style):
    return {
        'offset': offset,
        'length': length,
        'style': style
    }