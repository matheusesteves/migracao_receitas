from megadraft.key_generator import generate_random_key

def create_block_description(recipe):
    prepare_time = recipe['tempo_preparo']['prepare_time']
    prepare_time_hours = prepare_time['hours']
    prepare_time_minutes = prepare_time['minutes']
    return {
        'key': generate_random_key(),
        'text': '',
        'type': 'atomic',
        'depth': 0,
        'inlineStyleRanges': [],
        'entityRanges': [],
        'data': {
            'type': 'recipe-description',
            'description': '',
            'takenTime': 0 if prepare_time_hours == 0 else int(prepare_time_hours),
            'takenTimeUnit': 0 if prepare_time_minutes == 0 else int(prepare_time_minutes),
            'yield': recipe['rendimento'],
            'yieldType': {
                'label': 'porcoes',
                'singleLabel': 'porcao',
                'value': 'portions'
            }
        }
    }