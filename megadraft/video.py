from megadraft.key_generator import generate_random_key

def create_block_video(id_globo_videos):
    return {
        'key': generate_random_key(),
        'text':'',
        'type':'atomic',
        'depth': 0,
        'inlineStyleRanges':[],
        'entityRanges':[],
        'data': {
            'caption': '',
            'type': 'backstage-video',
            'identifier': id_globo_videos
        }
    }