def created_featured_image(recipe, width, height, image_type):
    featured_image = {}
    featured_image['id'] = ''
    featured_image['name'] = ''
    featured_image['rightsHolder'] = 'gcom'
    featured_image['url'] = recipe['thumb']
    featured_image['width'] = width
    featured_image['height'] = height
    featured_image['type'] = 'image/' + image_type.lower() if image_type else ''
    featured_image['isAnimated'] = False
    featured_image['thumborUrl'] = recipe['thumb']
    featured_image['crop'] = {
      'top':0,
      "left":0,
      "bottom": height,
      "right": width,
      "height": height,
      "width": width
    }
    return featured_image