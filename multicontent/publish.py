import requests
import json
from requests.exceptions import HTTPError
import base64

def publish(url, body, header, multicontent_id):
    try:
        response = None
        if multicontent_id:
            response = requests.put(url, data=body, headers=header)
        else:
            response = requests.post(url, data=body, headers=header)
        response.raise_for_status()
    except HTTPError as http_err:
        pass
    except Exception as exceptions:
        pass
    finally:
        return response.json(), response.status_code