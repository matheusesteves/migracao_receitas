from PIL import Image
import requests

def get_image_metadata(image_url):
    try:
        image = Image.open(requests.get(image_url, stream=True).raw)
        width, height = image.size
        return {
            'width': width,
            'height': height,
            'format': image.format
        }
    except Exception as e:
        return None