from megadraft.ingredients import create_block_ingredients
from megadraft.how_to import create_block_how_to
from megadraft.unstyled import create_block_unstyled
from megadraft.description import create_block_description
from megadraft.photo import create_block_photo
from megadraft.video import create_block_video
from semantic.annotation import create_annotation
from multicontent.featured import created_featured_image
from multicontent.metadata import get_image_metadata
from ckeditor.ckeditor import transform_ckeditor_core
import settings

def create_status(recipe):
    return {
        'slug': 'published',
        'type': 'published',
        'name': 'Publicado'
    }

def create_blocks_megadraft_how_to(list_block_steps):
    list_blocks = [create_block_how_to(block_steps) for block_steps in list_block_steps]
    return list_blocks

def create_blocks_megadraft_ingredients(list_block_ingredients):
    list_blocks = [create_block_ingredients(block_ingredients) for block_ingredients in list_block_ingredients]
    return list_blocks

def create_blocks_megadraft(recipe, image_width, image_height):
    block_description = create_block_description(recipe)
    blocks_ingredients = create_blocks_megadraft_ingredients(recipe['ingredients'])
    blocks_how_to = create_blocks_megadraft_how_to(recipe['steps'])
    block_video = create_block_video(recipe['id_globo_videos'])
    blocks_megadraft_body, entity_map = transform_ckeditor_core(recipe['body'])
    
    return {
        'blocks': 
            [create_block_unstyled('', [], []), block_video, 
             create_block_unstyled('', [], []), block_description,
             create_block_unstyled('', [], [])] + blocks_ingredients +
            [create_block_unstyled('', [], [])] + blocks_how_to +
            [create_block_unstyled('', [], [])] + blocks_megadraft_body,
        'entityMap': entity_map
    }

def create_request_body(recipe_core, multicontent_id):
    image_url = recipe_core['thumb']
    image_metadata = get_image_metadata(image_url)
    image_width = image_metadata['width'] if image_metadata else 0
    image_height = image_metadata['height'] if image_metadata else 0
    image_format = image_metadata['format'] if image_metadata else None

    multicontent = {}
    multicontent['accessControl'] = 0
    multicontent['comments'] = True
    multicontent['instantArticle'] = True
    multicontent['id'] = multicontent_id
    multicontent['issued'] = recipe_core['issued']
    multicontent['forcedCreated'] = recipe_core['created']
    multicontent['forcedModified'] = recipe_core['issued']
    multicontent['brandedContent'] = False
    multicontent['title'] = recipe_core['title']
    multicontent['slug'] = recipe_core['slug']
    multicontent['subtitle'] = recipe_core['subtitle']
    multicontent['tenantId'] = 'gshow'
    multicontent['contentType'] = settings.CONTENT_TYPE_ID
    multicontent['creator'] = 'Gshow'
    multicontent['category'] = settings.CATEGORY_ID
    multicontent['featuredImage'] = created_featured_image(recipe_core, image_width, image_height, image_format)
    multicontent['metaData'] = {
        'annotation' : create_annotation(recipe_core)
    }
    multicontent['status'] = create_status(recipe_core)
    multicontent['bodyData'] = create_blocks_megadraft(recipe_core, image_width, image_height)
    return multicontent