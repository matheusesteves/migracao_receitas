from unicodedata import normalize
import settings

def clean_title(title):
    ignored_items = ['-', ' ']
    title = title.strip()
    for item in ignored_items:
        title = title.replace(item, '')
    return normalize('NFKD', title).encode('ASCII', 'ignore').decode('ASCII').lower()

def get_id_by_title(title):
    title = clean_title(title)
    hashed_dict = {hash(clean_title(tag_item)):tag_value for tag_item,tag_value in settings.DICT_TAGS.items()}
    return hashed_dict.get(hash(title))