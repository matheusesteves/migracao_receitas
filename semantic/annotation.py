from semantic.semantic_generator import get_id_by_title

def create_annotation_list(list_tags_core, title_key):
    annotation_list = []
    for tag_core in list_tags_core:
        tag_title = tag_core[title_key]
        semantic_id = None
        if tag_title is not None and tag_title.strip() != '':
            semantic_id = get_id_by_title(tag_title)
            if semantic_id is not None:
                annotation_list.append(semantic_id)
    return annotation_list

def create_annotation(recipe):
    annotation = {}
    annotation_cozinha_list = create_annotation_list(recipe['trata_da_cozinha'], 'tag_title')
    annotation_dieta_list = create_annotation_list(recipe['trata_da_dieta'], 'tag_title')
    annotation_ocasiao_list = create_annotation_list(recipe['trata_da_ocasiao'], 'tag_title')
    annotation_modo_preparo_list = create_annotation_list(recipe['trata_do_modo_de_preparo'], 'tag_title')
    annotation_obra_list = create_annotation_list(recipe['editorias_secundarias'], 'editoria_secundaria_title')

    if annotation_cozinha_list and len(annotation_cozinha_list) > 0:
        annotation['gshow:trata_da_cozinha'] = annotation_cozinha_list
    if annotation_dieta_list and len(annotation_dieta_list) > 0:
        annotation['gshow:trata_da_dieta'] = annotation_dieta_list
    if annotation_ocasiao_list and len(annotation_ocasiao_list) > 0:
        annotation['gshow:trata_da_ocasiao'] = annotation_ocasiao_list
    if annotation_modo_preparo_list and len(annotation_modo_preparo_list) > 0:
        annotation['gshow:trata_do_modo_de_preparo'] = annotation_modo_preparo_list
    if annotation_obra_list and len(annotation_obra_list) > 0:
        annotation['gshow:trata_da_obra'] = annotation_obra_list

    return annotation