from crontab import CronTab
import os

def main():
    file_cronjobs = open('cronjobs.txt', 'r')
    list_cron_jobs = file_cronjobs.readlines()
    command_info = list_cron_jobs[0].replace('\n', '')
    list_cron_jobs.pop(0)
    command_info = command_info.split(' ')
    page_size = command_info[0]
    env = command_info[1]

    cron = CronTab(user=os.environ['USER'])
    for cron_job in list_cron_jobs:
        cron_job_components = cron_job.replace('\n', '').split(' ')
        page = cron_job_components[0]
        minute = int(cron_job_components[1])
        hour = int(cron_job_components[2])
        day = int(cron_job_components[3])
        month = int(cron_job_components[4])
        start_date = cron_job_components[5]
        end_date = cron_job_components[6]
        command = '{} {} {} {} {} {} {}'.format('/home/matheus.zanoto/projetosdextra/globosatcanais/migracao_receitas/migrate.py', env, page, page_size, env, start_date, end_date)
        slug = None
        if len(cron_job_components) > 7:
            slug = cron_job_components[7]
            command = '{} {}'.format(command, slug)
        create_cron_job(cron, command, minute, hour, day, month)

def create_cron_job(cron, command, minute, hour, day, month):
    job = cron.new(command=command)
    job.month.on(month)
    job.day.on(day)
    job.hour.on(hour)
    job.minute.on(minute)
    cron.write()

main()